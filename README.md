# Playbooks

Dieses Repository enthält verschiedenste Playbooks welche "quick and dirty" niedergeschrieben wurden oder enthalten test/example Code. Es kann also durchaus vorkommen das der Code etwas komisch aussieht allerdings werde ich versuchen hier nur Playbooks zu veröffentlichen die auch funktionieren (ohne Errors durchlaufen).

Hier eine kurze Auflistung der Playbooks und was man sich darunter vorstellen kann:

**openhab**
  - Installation von [OpenHAB](https://www.openhab.org)
  - Konfiguration von [Hyperion](https://hyperion-project.org/forum) und [Kodi](https://kodi.tv) als Thing
  - Konfiguration des "Stop" status von Kodi als Item
  - Konfiguration des aktivierten/deaktivierten Status von Hyperion
  - Konfiguration der Regeln, dass bei laufender Wiedergabe Hyperion aktiviert wird (Ambilight is aktiv) sowie dem umgekehrten fall.
  - **Besonderheit** Ich habe mich hier mal _wieder_ an den with_items versucht


**saned**\
  **Achtung**\
  Damit ein Client sich auch mit saned verbinden kann ist es nötig in `/etc/sane.d/net.conf` die IP des saned servers einzutragen. Das ist _nicht_ Bestandteil des Playbooks.
  - Installation vom saned
  - Konfiguration von saned um auf Netzwerken zu lauschen
