#!/bin/bash

function stop () {
  # stop all instances
  curl --header "Content-Type: application/json" -d '{"command" : "instance", "subcommand" : "stopInstance", "instance" : 0}' kodi:8090/json-rpc 
  curl --header "Content-Type: application/json" -d '{"command" : "instance", "subcommand" : "stopInstance", "instance" : 1}' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{"command" : "instance", "subcommand" : "stopInstance", "instance" : 2}' kodi:8090/json-rpc
}

function start () {
  # start all instances
  curl --header "Content-Type: application/json" -d '{"command" : "instance", "subcommand" : "startInstance", "instance" : 0}' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{"command" : "instance", "subcommand" : "startInstance", "instance" : 1}' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{"command" : "instance", "subcommand" : "startInstance", "instance" : 2}' kodi:8090/json-rpc
}
# set TV mode

function enable () {
  start
  #curl --header "Content-Type: application/json" -d '{"command" : "ledcolors", "subcommand" : "imagestream-start", "instance" : 0}' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command" : "instance", "subcommand" : "switchTo", "instance" : 0 }' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"GRABBER", "state":true }}' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"LEDDEVICE", "state":true }}' kodi:8090/json-rpc
  
  curl --header "Content-Type: application/json" -d '{ "command" : "instance", "subcommand" : "switchTo", "instance" : 1 }' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"GRABBER", "state":true }}' kodi:8090/json-rpc
  
  curl --header "Content-Type: application/json" -d '{ "command" : "instance", "subcommand" : "switchTo", "instance" : 2 }' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"GRABBER", "state":true }}' kodi:8090/json-rpc
}

function disable () {
  #curl --header "Content-Type: application/json" -d '{"command" : "ledcolors", "subcommand" : "imagestream-start", "instance" : 0}' kodi:8090/json-rpc
  # TV
  curl --header "Content-Type: application/json" -d '{ "command" : "instance", "subcommand" : "switchTo", "instance" : 0 }' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"LEDDEVICE", "state":false }}' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"GRABBER", "state":false }}' kodi:8090/json-rpc
  
  # strip
  curl --header "Content-Type: application/json" -d '{ "command" : "instance", "subcommand" : "switchTo", "instance" : 1 }' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"GRABBER", "state":false }}' kodi:8090/json-rpc
  
  # Pilze
  curl --header "Content-Type: application/json" -d '{ "command" : "instance", "subcommand" : "switchTo", "instance" : 2 }' kodi:8090/json-rpc
  curl --header "Content-Type: application/json" -d '{ "command":"componentstate", "componentstate":{ "component":"GRABBER", "state":false }}' kodi:8090/json-rpc
  stop
}

while getopts "sted" o; do
    case "${o}" in
        s)
          stop
            ;;
        t)
          start
            ;;
        e)
          enable
            ;;
        d)
          disable
            ;;
        *)
          exit 1
            ;;
    esac
done
